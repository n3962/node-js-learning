const http = require("http")
const fs = require("fs")

const reqListner = (req, res) => {
  if (req.url === "/") {
    res.write("<body>/ :- path</bode>")
    return res.end()
  }
  if (req.url === "/message") {
    fs.writeFile("message.txt", "DUMMY", (err) => {
      res.statusCode = 302
      res.setHeader("Location", "/")
      return res.end()
    })
  }
  res.write("<body>not / path</body>")
  res.end()
}

const server = http.createServer(reqListner)

server.listen(3000)
