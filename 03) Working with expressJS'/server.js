const path = require("path")

const express = require("express")
const bodyParser = require("body-parser")

const adminRoutes = require("./routes/admin")
const shopRouts = require("./routes/shop")

const app = express()

app.use(
  bodyParser.urlencoded({
    extended: false,
  })
)

app.use("/admin", adminRoutes)
app.use(shopRouts)

app.use((req, res, next) => {
  res.status(404).sendFile(path.join(__dirname, "views", "404.html"))
})

app.listen(3000, () => console.log("listenong on 3000"))
